# Guia 2

**Versión 1.0**

Archivo README.md para guía numero 2 de Algoritmos y Estructuras de Datos UTALCA

Profesor: Alejandro Mauricio Valdes Jimenez

Asignatura: Algoritmos y Estructuras de Datos

Autor: Benjamín Ignacio Fisher Pino

Fecha: 2/09/2021

---
## Resúmen del programa

Estos programas han sido desarrollados por Benjamín Fisher para resolver la guia numero 2 de Algoritmos y Estructuras de Datos. Mediante sencillos pasos el usuario podra encontrar la suma al cuadrado de una cierta cantidad de numeros ingresados por este, saber si un caracter ingresado por teclado es mayuscula, minuscula o invalido y por ultimo, almacenar y acceder a informacion de personas creadas por el usuario.

---
## Requerimientos

Para utilizar estos programas se requiere un computador con sistema operativo Linux Ubuntu version 20.04 L, un compilador g++ version 9.3.0 y la version 4.2.1 de make. En caso de no tener el compilador de g++ o make y no saber como descargarlos, abajo se encuentra una pequeña guia de como descargarlos.

### Instalacion de g++

Para instalar g++ debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install g++"

Luego debera introducir su contraseña de usuario y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para compilar codigos de c y c++.

### Instalacion de make

Para instalar make debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install make"

Luego debera introducir su contraseña de usuario y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para poder generar los ejecutables.

---
## Como instalarlo

Para instalar los programas debe dirigirse a la siguiente URL.

Link: https://gitlab.com/BenjaminFisherPino/guia-2.git

Tras ingresar al repositorio debe proceder a clonarlo ó a descargar las carpetas manualmente, ambas opciones funcionan.Luego debe dirigirse a travez de su terminal a la ruta donde se encuentran los archivos descargados. Una vez ahi, usted debe escoger entre las 3 carpetas, "code_1", "code_2" y "code_3" (estas carpetas corresponden al ejercicio 1 al 3 correspondientemente). Una vez se encuentre dentro de la carpeta deseada debe escribir el siguiente comando en su terminal.

"make" y posteriormente "./proyecto"

En caso de tener problemas asegurece de tener la version 9.3.0 de g++ y la version 4.2.1 de make

Puede revisar con "g++ --version" y "make --version" respectivamente.

Si siguio todos los pasos debiese estar listo para ejecutar el programa escogido!

---
## Funcionamiento del programa

### Codigo 1

El ejercicio uno consiste en obtener la suma de el cuadrado de los N numeros ingresados por el usuario. El codigo esta estructurado unicamente en base a una funcion main, la cual organiza todo el funcionamiento del programa. El comportamiento del programa es lineal y depende unicamente de los N datos ingresados por el usuario. El resultado de la ejecucion del programa, es la obtencion la suma del cuadrado de los datos.

### Codigo 2

El ejercicio dos consiste en analizar una N cantidad de caracteres ingresados por el usuario y identificar si son mayusculas, minusculas o invalidos. El programa esta separado en dos archivos, el archivo "main" (Main.cpp), el que permite establecer un orden a la hora de compilar y de ejecutar y el archivo "programa" (Programa.cpp), el cual nos permite ingresar y almacenar los caracteres, identificar si son mayuscula, minuscula o invalidos y imprimir el resultado obtenido. El comportamiento del programa es lineal y depende unicamente de la cantidad de datos ingresados por el usuario.

### Codigo 3

El ejercicio 3 consiste en el almacenamiento de personas con sus correspondientes datos personales (nombre, telefono, saldo, moroso). El programa esta separado en dos archivos, el main que permite mantener el orden de compilacion y de ejecucion y el archivo "persona" (Persona.cpp), el que permite crear un objeto y atribuirle los datos ingresados por el ususario.

Espero que este archivo README.md les haya ayudado a resolver sus dudas generales respecto a los programas. En caso de tener más dudas la direccion de correo electroncio al que se pueden dirigir es "bfisher20@alumnos.utalca.cl".

---

## License & Copyright

Ⓒ Benjamín Fisher Pino, Universidad de Talca
