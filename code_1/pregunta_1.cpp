#include <iostream>
#include <math.h>

using namespace std;

//FUNCION MAIN
int main () {
	
	//Variables
	int opt, num, suma;
	suma = 0;
	
	//Se define el tamaño de nuestro vector
	cout << "Ingrese el tamaño del arreglo" << endl;
	cin >> opt;
	int vector[opt]; 
	
	//Se agregan valores al vector
	for (int i = 0; i < opt ; i++){
		cout << "Ingrese un numero" << endl;
		cin >> num;
		vector[i] = num;
	}
	
	//Se obtiene la suma del cuadrado de todos
	//los numeros ingresados por el usuario
	
	for (int i = 0; i < opt ; i++){
		suma = suma + pow(vector[i], 2);
	}
	
	//Impresion de resultados en la terminal
	cout << "La suma del cuadrado de los " << opt 
	       << " numeros ingresados por el usuario corresponde a..." << endl
	       << "Resultado: " << suma << endl << endl;
    return 0;
}
