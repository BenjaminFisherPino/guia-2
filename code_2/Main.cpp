#include <iostream>
using namespace std;

#include "Programa.h"

//FUNCION MAIN
int main () {
	int size;
	
	//Se define el tamaño del vector
	cout << "Ingrese el tamaño de la lista" << endl;
	cin >> size;
	
	//Se instancia un objeto de tipo Programa y se le
	//pasa como parametro el tamaño de la lista
	Programa p = Programa(size);
	
	//Se instancian los metodos de la clase programa
	 p.llena_lista();
	 p.verifica_letras();
	 p.imprime_resultados();
	 
return 0;
}
