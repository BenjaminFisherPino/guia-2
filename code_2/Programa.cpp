#include <iostream>
#include <ctype.h>
using namespace std;

#include "Programa.h"

//CONSTRUCTOR CON UN PARAMETRO
Programa::Programa(int size){
	this->size = size;
	this->lista = new char[size];
	this->cont_may = 0;
	this->cont_min = 0;
	this->cont_inv = 0;
	}

//METODOS

//METODOS TIPO SETERS

//En esta funcion se llena la lista de tamaño size
//con caracteres ingresados por el usuario
void Programa::llena_lista(){
	for (int i = 0 ; i < size ; i++){
		cout << "Ingrese una letra" << endl;
		cin >> letra;
		lista[i] = letra;
	}
}

//En esta funcion se verifica si los caracteres son
//mayuscula, minuscula o si no son ninguna de las
//anteriores (caracteres invalidos)
void Programa::verifica_letras(){
	for (int i = 0; i < size ; i++){
		if (islower(lista[i])){
			this->cont_min++;
		}
		
		else if (isupper(lista[i])){
			this->cont_may++;
		}
		
		else{
			this->cont_inv++;
		}
	}
}

//En esta funcion se imprimen los resultados 
//obtenidos de la funcion anterior
void Programa::imprime_resultados(){
	cout << "Hay " << this->cont_may << " letras mayuscula" << endl;
	cout << "Hay " << this->cont_min << " letras minusculas" << endl;
	cout << "Hay " << this->cont_inv << " caracteres no identificados" << endl;
}
