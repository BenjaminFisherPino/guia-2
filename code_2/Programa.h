#ifndef PROGRAMA_H
#define PROGRAMA_H

class Programa {

    //ATRIBUTOS
    private:
		char* lista;
		char letra;
		int cont_may, cont_min, cont_inv, size;
		
    public:
    //CONSTRUCTOR CON UN PARAMETRO
		Programa(int size);

    //METODOS
		
		//Seters
		void llena_lista();
		void verifica_letras();
		void imprime_resultados();
};
#endif
