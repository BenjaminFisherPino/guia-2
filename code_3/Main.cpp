#include <iostream>
using namespace std;

#include "Persona.h"

//FUNCION MAIN
int main () {
	
	//Variables
	string nombre, telefono;
	int resp, opc, saldo, temp, num_personas, cont, indice;
	bool moroso;
	
	resp = 1;
	cont = 0;
	cout << "Bienvenido" << endl << endl;
	cout << "Ingrese la cantidad de personas" << endl;
	cin >> num_personas;
	
	//Se define el tamaño del vector lista.
	Persona lista[num_personas];
	
	//CICLO PRINCIPAL
	while (resp == 1 ){
		cout << "Ingrese 1 para añadir personas" << endl
		       << "Ingrese 2 para imprimir los datos de una persona" << endl
		       << "Ingrese 3 para salir del programa" << endl << endl;
		       
		cin >> opc;
		
		//Este ciclo nos permite agregar personas hasta completar
		//el tamaño de nuestro vector lista previamente definido.
		if(opc == 1){
			if (cont < num_personas){
			cout << "Ingrese el nombre de la persona..." << endl;
			cin >> nombre;
			cout << "Ingrese el telefono de la persona..." << endl;
			cin >> telefono;
			cout << "Ingrese el saldo de la persona" << endl;
			cin >> saldo;
			cout << "La persona tiene deudas pendientes?" << endl
				   << "Ingrese 1 si la persona tiene deudas" << endl
				   << "Presione enter si no debe deudas" << endl;
				   
			cin >> temp;
			
			if (temp == 1){
				moroso = true;
			}
			
			else {
				moroso = false;
			}
			
			//Instancia de objeto de tipo Persona.
			Persona p = Persona(nombre, telefono, saldo, moroso);
			
			//Se añade a la lista
			lista[cont] = p;
			
			//Con este contador nos aseguramos de no hacer mas instancias
			//de la clase Persona de las que nuestra lista puede almacenar.
			cont++;
			}
			
			//En caso de que se haya completado el limite de personas
			//el usuario es desviado.
			else{
				cout << "No puede agregar mas personas" << endl << endl;
			}
	}
		
		//Esta opcion nos permite imprimir los datos de una persona
		//en base a su indice, la idea es que el usuario ingrese un
		//numero entre el 1 hasta n, siendo n el tamaño de la lista.
		else if (opc == 2){
			cout << "Ingrese el indice de la persona" << endl;
			cin >> indice;
			
			// "indice - 1" sirve para que el usuario pueda ingresar al
			//puesto 0 del arreglo escribiendo 1. Mas que nada es por un
			//tema de comodidad para el usuario.
			indice = indice - 1;
			cout << endl << "NOMBRE: " << lista[indice].get_nombre()<< endl;
			cout << "TELEFONO: " << lista[indice].get_telefono() << endl;
			cout << "SALDO DISPONIBLE: " << lista[indice].get_saldo() << endl;
			
			//En caso de que la persona tenga su atributo moroso como
			// "true", se imprime este texto.
			if (lista[indice].get_moroso()){
				cout << "LA PERSONA TIENE DEUDAS PENDIENTES" << endl << endl;
			}
			
			//En caso contrario se imprime este texto.
			else{
				cout << "LA PERSONA NO TIENE DEUDAS" << endl << endl;
			}
	}
		
		//Esta opcion nos permite salir del programa tras
		//finalizar nuestro ciclo principal while
		else if (opc == 3){
			cout << "Hasta luego!" << endl;
			resp = 0;
		}
		
		//Esta opcion nos permite ahorrarnos un problema durante
		//la ejecucion del programa, en caso de ingresar una opcion
		//distinta de 1, 2 o 3 el programa se cerraria automaticamente.
		//Por eso es muy util la utilizacion de else.
		else{
			cout << "Opcion invalida...";
		}
	}
	
    return 0;
}
