#include <iostream>
using namespace std;

#include "Persona.h"

//CONSTRUCTOR POR DEFECTO
Persona::Persona(){
	this->nombre = "S/N";
	this->telefono = "S/N";
	this->saldo = 0;
	this->moroso = false;
	}

//CONSTRUCTOR CON PARAMETROS
Persona::Persona(string nombre, string telefono, int saldo, bool m){
	this->nombre = nombre;
	this->telefono = telefono;
	this->saldo = saldo;
	this->moroso = m;
}

//METODOS

//METODOS DE TIPO SETERS
void Persona::set_nombre(string nombre){
	this->nombre = nombre;
}

void Persona::set_telefono(string telefono){
	this->telefono = telefono;
}

void Persona::set_saldo(int saldo){
	this->saldo = saldo;
}

void Persona::set_moroso(bool m){
	this->moroso = m;
}

//METODOS DE TIPO GETERS
string Persona::get_nombre(){
	return this->nombre;
}

string Persona::get_telefono(){
	return this->telefono;
}

int Persona::get_saldo(){
	return this->saldo;
}

bool Persona::get_moroso(){
	return this->moroso;
}
