#ifndef PERSONA_H
#define PERSONA_H

class Persona {

    //ATRIBUTOS
    private:
		string nombre, telefono;
		bool moroso;
		int saldo;
		
    public:
    //CONSTRUCTOR POR DEFECTO
		Persona();
        
    //CONSTRUCTOR CON PARAMETROS
		Persona(string nombre, string telefono, int saldo, bool m);
		
    //METODOS
    
    //Seters
    void set_nombre(string nombre);
    void set_telefono(string telefono);
    void set_saldo(int saldo);
    void set_moroso(bool m);
    
    //Geters
    string get_nombre();
    string get_telefono();
    int get_saldo();
    bool get_moroso();

};
#endif
